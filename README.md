# Gitlab TODO list

A small script to print a list of Merge Requests requiring my attention.

## How to use

Create a `config.yaml` file similar to [config.sample.yaml](config.sample.yaml) with the Gitlab instances you want to check.

```yaml
---
instances:
  - host: URL to the Gitlab instance
    token: A user token with API access
    namespaces:
      - list of namespaces that should be included
      - if nothing is provided, all projects are shown
      - group/subgroup/project
      - group/subgroup
      - group
```

Running the script will output a table with intersting data about the Merge Requests you should check.

```bash
$ python3 -m gitlab_todo
      ID  Title                                                   Last Upd    State    CI       Author        Assignee            D.Res.    URL
--------  ------------------------------------------------------  ----------  -------  -------  ------------  ------------------  --------  -------------------------------------------------------------------------------------
81409307  Correct kcidb adapter revision tree name                2 days      opened   success  mh21          inakimalerba,veruu  False     https://gitlab.com/cki-project/cki-tools/-/merge_requests/246
81255149  kcidb adapter: drop assert on missing kernel_arch       2 days      opened   success  jracek        inakimalerba        False     https://gitlab.com/cki-project/cki-tools/-/merge_requests/245
77717452  WIP Generate list of recipients.                        32 days     opened   failed   inakimalerba                      True      https://gitlab.com/cki-project/datawarehouse/-/merge_requests/224
69844240  Draft: Create Enum with KCIDB messages status.          73 days     opened   failed   inakimalerba  inakimalerba        True      https://gitlab.com/cki-project/datawarehouse/-/merge_requests/174
```
