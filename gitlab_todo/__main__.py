#! /usr/bin/python3
"""List of Gitlab To Dos."""
import argparse
import datetime
import pathlib

from concurrent.futures import ThreadPoolExecutor
from concurrent.futures import as_completed
import dateutil.parser
import gitlab
import yaml
from tabulate import tabulate


MAX_CONCURRENT_WORKERS = 10


class MergeRequest:
    """Merge Request object."""

    header = ['ID', 'Title', 'Last Upd', 'State', 'CI', 'Author', 'Assignee', 'D.Res.', 'URL']

    def __init__(self, gl, obj):
        """Initialize."""
        self.project = gl.projects.get(obj.project_id)
        self.obj = self.project.mergerequests.get(obj.iid)

    @property
    def values(self):
        """Return a dict with the MR values."""
        return {
            'ID': self.obj.id,
            'Title': self.obj.title,
            'Last Upd': self.last_update,
            'State': self.obj.state,
            'CI': self.pipeline_state,
            'Author': self.obj.author['username'],
            'Assignee': self.assignees,
            'D.Res.': self.obj.blocking_discussions_resolved,
            'URL': self.obj.web_url,
        }

    def __iter__(self):
        """Return iterable to populate the table, sorted according to the header list."""
        return iter([self.values[key] for key in self.header])

    @property
    def last_update(self):
        """Return time since last update."""
        difference = (
            datetime.datetime.now(tz=dateutil.tz.UTC) -
            dateutil.parser.parse(self.obj.updated_at)
        )
        return f'{difference.days} days'

    @property
    def updated_at(self):
        """Return updated_at datetime object."""
        return dateutil.parser.parse(self.obj.updated_at)

    @property
    def assignees(self):
        """Return string with assignees usernames."""
        return ','.join([
            user['username'] for user in self.obj.assignees
        ])

    @property
    def pipeline_state(self):
        """Return pipeline status."""
        if not self.obj.pipeline:
            return None
        return self.obj.pipeline['status']

    def __gt__(self, other):
        """Order by updated_at timestamp."""
        #return self.obj.id > other.obj.id
        return self.obj.updated_at > other.obj.updated_at


class MRList:
    """List of MRs removing duplicates."""

    def __init__(self):
        """Init."""
        self.items = {}

    def extend(self, gl_instance, items):
        """Add new mrs to the list."""
        for merge_request in items:
            self.items.update({merge_request.id: (gl_instance, merge_request)})

    def fetch(self):
        """Update with remote data."""
        futures = {}
        with ThreadPoolExecutor(max_workers=MAX_CONCURRENT_WORKERS) as executor:
            futures = {
                executor.submit(MergeRequest, gl_instance, merge_request): merge_request.id
                for gl_instance, merge_request in self.items.values()
            }

            for future in as_completed(futures):
                merge_request = future.result()
                merge_request_id = futures[future]
                self.items.update(
                    {merge_request_id: merge_request}
                )

    def __iter__(self):
        """Return an iterator sorted by id."""
        return iter(
            sorted(self.items.values(), reverse=True)
        )


def filtered_query(query, namespaces=None):
    """Remove the MRs not matching the namespaces."""
    if not namespaces:
        return query

    return [
        merge_request for merge_request in query
        if any([namespace in merge_request.web_url for namespace in namespaces])
    ]


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        description='Get GitLab MRs needing my attention.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--config-file', default='config.yaml', help='Path to config file.')

    return parser.parse_args()


def main():
    """Run script."""
    args = parse_args()
    config = yaml.safe_load(pathlib.Path(args.config_file).read_text())

    merge_requests = MRList()

    for instance in config.get('instances', []):
        gl_instance = gitlab.Gitlab(instance['host'], instance['token'])
        username = gl_instance.http_get('/user')['username']  # Couldn't find this method on the lib..

        queries = [
            # My MergeRequests.
            *gl_instance.mergerequests.list(as_list=True, author_username=username, state='opened'),
            # MergeRequests assigned to me.
            *gl_instance.mergerequests.list(as_list=True, scope='assigned_to_me', state='opened'),
        ]

        # Exclude MRs from unwanted namespaces
        queries = filtered_query(queries, instance.get('namespaces'))

        merge_requests.extend(gl_instance, queries)

    merge_requests.fetch()

    print(
        tabulate(merge_requests, headers=MergeRequest.header)
    )


if __name__ == '__main__':
    main()
